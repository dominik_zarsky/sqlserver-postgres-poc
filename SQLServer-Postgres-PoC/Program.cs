﻿using Microsoft.EntityFrameworkCore;
using SQLServer_Postgres_PoC.Data;
using SQLServer_Postgres_PoC.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SQLServer_Postgres_PoC
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Invalid arguments");
                return;
            }

            Console.WriteLine("SQL Server to PostgreSQL data transfer Proof of Concept script");
            using var sqlContext = new SqlServerContext();
            using var postgresContext = new PostgreContext();

            switch (args[0])
            {
                case "seed-users":
                    Console.WriteLine("Seeding users");
                    await SeedUsers(sqlContext, 100);
                    break;
                case "seed-properties":
                    Console.WriteLine("Seeding user properties");
                    await SeedProperties(sqlContext);
                    break;
                case "migrate":
                    Console.WriteLine("Migrating SQL Server data to Postgres");
                    Migrate(sqlContext, postgresContext);
                    break;
                case "test":
                    Console.WriteLine("Testing connection");
                    var pgUser = postgresContext.Set<User>().FirstOrDefaultAsync();
                    var sqlServerUser = sqlContext.Set<User>().FirstOrDefaultAsync();
                    Console.WriteLine("PG: " + pgUser.Result.Username);
                    Console.WriteLine("SQL Server: " + sqlServerUser.Result.Username);
                    break;
                default:
                    throw new NotImplementedException();
            }

        }

        public static void Migrate(SqlServerContext sqlServerContext, PostgreContext postgresContext)
        {
            var users = sqlServerContext.Set<User>().AsQueryable();
            var properties = sqlServerContext.Set<Property>().AsQueryable();

            postgresContext.Set<User>().AddRange(users);
            postgresContext.Set<Property>().AddRange(properties);

            postgresContext.SaveChanges();
        }

        private static async Task SeedUsers(SqlServerContext context, int count)
        {
            var users = new List<User>();
            for (var x = 0; x <= count; x++)
            {
                users.Add(new User { Username = "User " + x, Password = "123456" });
            }

            await context.Set<User>().AddRangeAsync(users);
            await context.SaveChangesAsync();
        }

        private static async Task SeedProperties(SqlServerContext context)
        {
            var users = context.Set<User>().AsQueryable();
            var properties = new List<Property>();

            foreach (var user in users)
            {
                properties.Add(new Property { Name = "Property for user" + user.Username, Value = new Random().Next(1, 5).ToString(), UserID = user.UserID });
            }

            await context.Set<Property>().AddRangeAsync(properties);
            await context.SaveChangesAsync();
        }
    }
}
