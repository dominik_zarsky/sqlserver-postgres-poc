﻿using System;

namespace SQLServer_Postgres_PoC.Models
{
    public class Property
    {
        public int PropertyID { get; set; }
        public int? UserID { get; set; }
        public User? User { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
