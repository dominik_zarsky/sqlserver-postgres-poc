﻿using System;

namespace SQLServer_Postgres_PoC.Models
{
    public class User
    {
        public int UserID { get; set; }
        public bool IsDeleted { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
