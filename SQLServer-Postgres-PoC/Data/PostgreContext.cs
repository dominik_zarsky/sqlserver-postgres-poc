﻿using Microsoft.EntityFrameworkCore;

namespace SQLServer_Postgres_PoC.Data
{
    public class PostgreContext : SqlServerContext
    {
        private static DbConnectionHelpers _helpers = new DbConnectionHelpers();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_helpers.GetPostgresConnectionString());
        }
    }
}
