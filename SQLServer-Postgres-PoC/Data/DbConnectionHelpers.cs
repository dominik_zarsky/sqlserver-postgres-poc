﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace SQLServer_Postgres_PoC.Data
{
    public class DbConnectionHelpers
    {
        private static IConfiguration _configuration = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", true, true)
               .AddUserSecrets<Program>()
               .Build();

        public string GetSqlServerConnectionString()
        {
            return _configuration.GetConnectionString("sqlserver");
        }

        public string GetPostgresConnectionString()
        {
            return _configuration.GetConnectionString("postgres");
        }

    }
}
