﻿using Microsoft.EntityFrameworkCore;
using SQLServer_Postgres_PoC.Models;
using System;

namespace SQLServer_Postgres_PoC.Data
{
    public class SqlServerContext : DbContext
    {
        private static DbConnectionHelpers _helpers = new DbConnectionHelpers();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_helpers.GetSqlServerConnectionString());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Property> Properties { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Property>()
                .HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserID);

            builder.Entity<User>()
                .Property(x => x.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Entity<User>()
                .Property(x => x.DateUpdated)
                .HasDefaultValue(DateTime.Now);

            builder.Entity<Property>()
                .Property(x => x.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Entity<Property>()
                .Property(x => x.DateUpdated)
                .HasDefaultValue(DateTime.Now);

            builder.Entity<User>()
                .Property(x => x.IsDeleted)
                .HasDefaultValue(false);

            builder.Entity<User>()
                .HasKey(x => x.UserID);

            builder.Entity<Property>()
                .HasKey(x => x.PropertyID);
        }

    }
}
